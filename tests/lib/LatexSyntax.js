/**
 * @fileoverview Tests of LaTeX syntax structures.
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous project team (http://texnous.org) 2016-2017
 * @license LGPL-3.0
 *
 * This unit test is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This unit test is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this unit
 * test; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA.
 */

"use strict";

const Latex = require("@texnous/latex"); // general LaTeX definitions
const LatexSyntax = require("../../sources/lib/LatexSyntax"); // LaTeX syntax structures

let
	item,
	parameter,
	latexSyntax;

/**
 * LatexSyntax namespace tests
 * @author Kirill Chuvilin <k.chuvilin@texnous.org>
 */
module.exports = {
	"Item": {
		"constructor": test => {
			test.throws(() => new LatexSyntax.Item(""));
			test.throws(() => new LatexSyntax.Item(null));
			test.doesNotThrow(() => new LatexSyntax.Item());
			test.doesNotThrow(() => new LatexSyntax.Item({ }));
			test.doesNotThrow(() => new LatexSyntax.Item([ ]));
			test.doesNotThrow(() => new LatexSyntax.Item({ lexeme: null }));
			test.doesNotThrow(() => new LatexSyntax.Item({ lexeme: Latex.Lexeme.CHAR }));
			test.throws(() => new LatexSyntax.Item({ lexeme: "" }));
			test.throws(() => new LatexSyntax.Item({ modes: null }));
			test.doesNotThrow(() => new LatexSyntax.Item({ modes: [ ] }));
			test.doesNotThrow(() => new LatexSyntax.Item({ modes: { }}));
			test.throws(() => new LatexSyntax.Item({ modes: { MODE: true } }));
			test.doesNotThrow(() =>	item = new LatexSyntax.Item({
				lexeme: Latex.Lexeme.CHAR, modes: { MATH: true }
			}));
			test.doesNotThrow(() => new LatexSyntax.Item(item));
			test.done();
		},
		"equals": test => {
			test.equal((new LatexSyntax.Item({ })).equals(new LatexSyntax.Item({ })), true);
			test.equal((new LatexSyntax.Item({ })).equals(new LatexSyntax.Item({ lexeme: null, modes: [ ] })), true);
			test.equal(item.equals(new LatexSyntax.Item({ })), false);
			test.equal(item.equals(item), true);
			test.done();
		}
	},
	"Parameter": {
		"constructor": test => {
			test.doesNotThrow(() => new LatexSyntax.Parameter());
			test.throws(() => new LatexSyntax.Parameter({ operations: null }));
			test.throws(() => new LatexSyntax.Parameter({ operations: { } }));
			test.doesNotThrow(() => new LatexSyntax.Parameter({ operations: [ ] }));
			test.doesNotThrow(() => new LatexSyntax.Parameter(item));
			test.doesNotThrow(() => parameter = new LatexSyntax.Parameter(item));
			test.done();
		},
		"equals": test => {
			test.equal((new LatexSyntax.Parameter({ })).equals(new LatexSyntax.Parameter({ })), true);
			test.equal(
				(new LatexSyntax.Parameter({ })).equals(new LatexSyntax.Parameter({ lexeme: null, modes: [ ] })),
				true
			);
			test.equal(parameter.equals(new LatexSyntax.Item({ })), false);
			test.equal(parameter.equals(parameter), true);
			test.done();
		}
	},
	"LatexSyntax": {
		"constructor": test => {
			test.doesNotThrow(() => latexSyntax = new LatexSyntax());
			test.done();
		},
		"loadPackage": test => {
			test.doesNotThrow(() => latexSyntax.loadPackage("test", {
				symbols: [{
					pattern: "\\\\"
				}],
				commands: [{
					name: "author",
					pattern: "[#1]#2",
					modes: { TEXT: true },
					parameters: [{}, {}],
					operations: []
				}, {
					name: "author",
					pattern: " [#1]#2",
					modes: { TEXT: true },
					parameters: [{}, {}],
					operations: []
				}, {
					name: "author",
					pattern: "#1",
					modes: { TEXT: true },
					parameters: [{}],
					operations: []
				}, {
					name: "document",
					modes: { TEXT: true }
				}, {
					name: "enddocument",
					modes: { TEXT: true }
				}],
				environments: [{
					name: "document",
					modes: { TEXT: true }
				}]
			}));
			test.done();
		},
		"symbols": test => {
			test.equal(latexSyntax.symbols(new Latex.State({ }), "\\").length, 1);
			test.equal(latexSyntax.symbols(new Latex.State({ MATH: true }), "\\").length, 1);
			test.equal(latexSyntax.symbols(new Latex.State({ }), "a").length, 0);
			test.done();
		},
		"commands": test => {
			test.equal(latexSyntax.commands(new Latex.State({ }), "test").length, 0);
			test.equal(latexSyntax.commands(new Latex.State({ }), "author").length, 3);
			test.equal(latexSyntax.commands(new Latex.State({ TEXT: true }), "author").length, 3);
			test.equal(latexSyntax.commands(new Latex.State({ TEXT: false }), "author").length, 0);
			test.equal(latexSyntax.commands(new Latex.State({ MATH: true }), "author").length, 3);
			test.done();
		},
		"environments": test => {
			test.equal(latexSyntax.environments(new Latex.State({ }), "test").length, 0);
			test.equal(latexSyntax.environments(new Latex.State({ }), "document").length, 1);
			test.equal(latexSyntax.environments(new Latex.State({ TEXT: true }), "document").length, 1);
			test.equal(latexSyntax.environments(new Latex.State({ TEXT: false }), "document").length, 0);
			test.done();
		},
		"unloadPackage": test => {
			test.doesNotThrow(() => latexSyntax.unloadPackage("test"));
			test.equal(latexSyntax.symbols(new Latex.State({ }), "\\").length, 0);
			test.equal(latexSyntax.symbols(new Latex.State({ MATH: true }), "\\").length, 0);
			test.equal(latexSyntax.commands(new Latex.State({ }), "author").length, 0);
			test.equal(latexSyntax.commands(new Latex.State({ TEXT: true }), "author").length, 0);
			test.equal(latexSyntax.commands(new Latex.State({ MATH: true }), "author").length, 0);
			test.equal(latexSyntax.environments(new Latex.State({ }), "document").length, 0);
			test.equal(latexSyntax.environments(new Latex.State({ TEXT: true }), "document").length, 0);
			test.done();
		}
	}
};
